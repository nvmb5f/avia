import React from 'react';
import ReactDOM from 'react-dom';

import './index.scss';
import App from "./app/App";

const root = (
    <React.StrictMode>
      <App/>
    </React.StrictMode>
);

ReactDOM.render(root, document.getElementById('root'));
