import React from 'react';
import './Navbar.scss';

import logo from 'assets/logo.svg';

const Navbar = () => {
  return (
      <div className="navbar-container">
        <img src={logo} className="logo" alt="logo"/>
        <div className="settings">
          <img src={logo} className="currency" alt="currency"/>
          <img src={logo} className="account" alt="account"/>
        </div>
      </div>
  );
};

export default Navbar;