import React from 'react';
import './SearchBar.scss';

import SearchBarDateInput from "./units/SearchBarDateInput";
import SearchBarTextInput from "./units/SearchBarTextInput";


const SearchBar = () => {
  return (
      <div className="searchBar-container">
        <SearchBarTextInput label="Откуда"/>
        <SearchBarTextInput label="Куда"/>
        <SearchBarDateInput label="Вылет"/>
        <SearchBarDateInput label="Прилет"/>
        <button>Поиск</button>
      </div>
  );
};

export default SearchBar;
