import React from 'react';

const SearchBarTextInput = ({label}: any) => {

  const handleInput = () => {
  };

  return (
      <label>
        <span>{label}</span>
        <div>
          <input placeholder={label} onChange={handleInput}/>
        </div>
      </label>
  );
};

export default SearchBarTextInput;