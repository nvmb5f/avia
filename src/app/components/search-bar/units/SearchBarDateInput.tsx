import React from 'react';
import Calendar from "inputs/calendar/Calendar";

const SearchBarDateInput = ({label}: any) => {

  const INTERVAL_IN_MONTHS = 3;
  const currentDate = new Date();

  const fromDate = new Date(currentDate);
  const toDate = new Date(currentDate.setMonth(fromDate.getMonth() + INTERVAL_IN_MONTHS));

  return (
      <label>
        <span>{label}</span>
        <Calendar placeholder={label} from={fromDate} to={toDate} />
      </label>
  );
};

export default SearchBarDateInput;