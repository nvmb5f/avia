import React from "react";
import {Ticket} from "models/flight/Ticket";
import './TicketSearchResults.scss';
import {TicketCard} from "./ticket-card/TicketCard";


export const TicketSearchResults = ({tickets}: { tickets: Ticket[] }) => {
  return (
      <div className="search-result-panel">
        {tickets.map((ticket, i) => <TicketCard ticket={ticket} key={i}/>)}
      </div>
  );
};
