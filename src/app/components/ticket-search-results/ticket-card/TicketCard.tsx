import {Ticket} from "models/flight/Ticket";
import React from "react";
import {AirlinesBlock} from "./blocks/AirlinesBlock";
import {FlightBlock} from "./blocks/FlightBlock";

export const TicketCard = (props) => {
  const ticket: Ticket = props.ticket;

  return (
      <div className="ticket-card">
        <div className="ticket-card--left">
          <button>
            <div>Купить билет</div>
            <div>{ticket.price}</div>
          </button>
        </div>
        <div className="ticket-card--right">
          <div className="ticket-card--right--header">
            <AirlinesBlock flights={ticket.flights}/>
          </div>
          <div className="ticket-card--right--body">
            <FlightBlock ticket={ticket}/>
          </div>
        </div>
      </div>
  );
}


