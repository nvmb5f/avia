import {Ticket} from "models/flight/Ticket";
import {FlightPoint} from "models/flight/FlightPoint";
import {Flight} from "models/flight/Flight";
import imgPlaneMock from "assets/logo.svg";
import React from "react";

export const FlightBlock = ({ticket}: { ticket: Ticket }) => {
  const origin = ticket.flights[0];
  const destination = ticket.flights[ticket.flights.length - 1];

  return (
      <div className="flight-summary--container">
        <FlightPointUnit flightPoint={origin.departure}/>
        <FlightSchemeUnit flights={ticket.flights}/>
        <FlightPointUnit flightPoint={destination.arrival}/>
      </div>
  )
}


const FlightPointUnit = ({flightPoint}: { flightPoint: FlightPoint }) => {
  const parseTime = (date) => date.toLocaleTimeString('ru-RU').substring(0, 5);
  const parseDate = (date) => date.toLocaleDateString('ru-RU');

  return (
      <div>
        <h2>{parseTime(flightPoint.date)}</h2>
        <div>
          <div>{parseDate(flightPoint.date)}</div>
          <div>{flightPoint.city}</div>
        </div>
      </div>
  );
};

const FlightSchemeUnit = ({flights}: { flights: Flight[] }) => {
  const departureDate = flights[0].departure.date;
  const arrivalDate = flights[flights.length - 1].arrival.date;
  const flightTimeInMs = arrivalDate.getTime() - departureDate.getTime();

  const airportSequence: string[] = [flights[0].departure.airportIATA]
      .concat(flights.map(f => f.arrival.airportIATA));

  const calcFlightTime = (ms): string => {
    const mins = ms / (60 * 1000);
    const hours = mins / 60;
    const days = hours / 24;
    return `В пути ${Math.floor(days % 7)} д ${Math.floor(hours % 24)} ч ${mins % 60} мин`;
  }

  return (
      <div className="flight-scheme">
        <div className="flight-scheme--time">
          <img src={imgPlaneMock} alt="Взлет"/>
          <span>{calcFlightTime(flightTimeInMs)}</span>
          <img src={imgPlaneMock} alt="Посадка"/>
        </div>
        <hr className="flight-scheme--route"/>
        <div className="flight-scheme--airports">
          {airportSequence
              .map((airport, i) => <div key={i}>{airport}</div>)}
        </div>
      </div>
  )
};