import {Flight} from "models/flight/Flight";
import React from "react";

export const AirlinesBlock = ({flights}: { flights: Flight[] }) => {
  const buildURL = (iataCode) => `http://pics.avs.io/night_square/36/36/${iataCode}.png`;
  return (
      <div className="airlines-logos">
        {flights.map((flight, i) => <img className="airline-logo"
                                         src={buildURL(flight.airlineIATA)}
                                         title={flight.airline}
                                         alt={flight.airline}
                                         key={i}/>)
        }
      </div>
  )
}