import {BaggagePolicy} from "models/baggage/BaggagePolicy";
import {BaggageType} from "models/baggage/BaggageType";

export const BAGGAGE_POLICY_1: BaggagePolicy = [
  {
    type: BaggageType.CARRY_ON_BAGGAGE,
    price: 0,
    places: 1,
    maxWeight: 10,
    maxLength: 55,
    maxWidth: 40,
    maxHeight: 23,
  }
]

export const BAGGAGE_POLICY_2: BaggagePolicy = [
  {
    type: BaggageType.CARRY_ON_BAGGAGE,
    price: 0,
    places: 1,
    maxWeight: 10,
    maxLength: 55,
    maxWidth: 40,
    maxHeight: 23,
  },
  {
    type: BaggageType.BAGGAGE,
    price: 1734,
    places: 1,
    maxWeight: 15,
    maxLength: 65,
    maxWidth: 50,
    maxHeight: 33,
  }
]