/***********************************
 *               1                 *
 ***********************************/
import {FlightPoint} from "models/flight/FlightPoint";

export const FLIGHT_POINT_1_DEP: FlightPoint = {
  date: new Date(2021, 5, 1, 14, 0),
  country: "Россия",
  city: "Санкт-Петербург",
  airport: "Аэропорт Пулково",
  airportIATA: "LED",
}

export const FLIGHT_POINT_1_ARR: FlightPoint = {
  date: new Date(2021, 5, 1, 17, 45),
  country: "Турция",
  city: "Анталья",
  airport: "Анталья",
  airportIATA: "AYT",
}

/***********************************
 *               2                 *
 ***********************************/
export const FLIGHT_POINT_2_1_DEP: FlightPoint = {
  date: new Date(2021, 5, 1, 6, 30),
  country: "Россия",
  city: "Санкт-Петербург",
  airport: "Пулково",
  airportIATA: "LED",
}

export const FLIGHT_POINT_2_1_ARR: FlightPoint = {
  date: new Date(2021, 5, 1, 9, 50),
  country: "Италия",
  city: "Милан",
  airport: "Орио Аль Серио",
  airportIATA: "BGY",
}

export const FLIGHT_POINT_2_2_DEP: FlightPoint = {
  date: new Date(2021, 5, 1, 16, 35),
  country: "Италия",
  city: "Милан",
  airport: "Орио Аль Серио",
  airportIATA: "BGY",
}

export const FLIGHT_POINT_2_2_ARR: FlightPoint = {
  date: new Date(2021, 5, 1, 17, 20),
  country: "Германия",
  city: "Гамбург",
  airport: "Гамбург",
  airportIATA: "HAM",
}

export const FLIGHT_POINT_2_3_DEP: FlightPoint = {
  date: new Date(2021, 5, 2, 10, 45),
  country: "Германия",
  city: "Гамбург",
  airport: "Гамбург",
  airportIATA: "HAM",
}

export const FLIGHT_POINT_2_3_ARR: FlightPoint = {
  date: new Date(2021, 5, 2, 15, 20),
  country: "Турция",
  city: "Анталья",
  airport: "Анталья",
  airportIATA: "AYT",
}
