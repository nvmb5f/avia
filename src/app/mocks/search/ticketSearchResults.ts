import {Ticket} from "models/flight/Ticket";
import {FLIGHT_1, FLIGHT_2_1, FLIGHT_2_2, FLIGHT_2_3} from "./flights";
import {BAGGAGE_POLICY_1, BAGGAGE_POLICY_2} from "./baggage";

const TICKET_1: Ticket = {
  price: 12096,
  flights: [FLIGHT_1],
  baggagePolicies: [BAGGAGE_POLICY_1, BAGGAGE_POLICY_2]
}

const TICKET_2: Ticket = {
  price: 10118,
  flights: [FLIGHT_2_1, FLIGHT_2_2, FLIGHT_2_3],
  baggagePolicies: [BAGGAGE_POLICY_1],
}

export const TICKETS: Ticket[] = [TICKET_1, TICKET_2]