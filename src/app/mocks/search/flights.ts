import {Flight} from "models/flight/Flight";
import {
  FLIGHT_POINT_1_ARR,
  FLIGHT_POINT_1_DEP,
  FLIGHT_POINT_2_1_ARR,
  FLIGHT_POINT_2_1_DEP,
  FLIGHT_POINT_2_2_ARR,
  FLIGHT_POINT_2_2_DEP,
  FLIGHT_POINT_2_3_ARR,
  FLIGHT_POINT_2_3_DEP
} from "./flight-points";

export const FLIGHT_1: Flight = {
  number: "S7-4452",
  departure: FLIGHT_POINT_1_DEP,
  arrival: FLIGHT_POINT_1_ARR,
  airline: "S7 Airlines",
  airlineIATA: "S7",
}

export const FLIGHT_2_1: Flight = {
  number: "W6-6515",
  departure: FLIGHT_POINT_2_1_DEP,
  arrival: FLIGHT_POINT_2_1_ARR,
  airline: "Wizz Air",
  airlineIATA: "W6",
}

export const FLIGHT_2_2: Flight = {
  number: "FR-5501",
  departure: FLIGHT_POINT_2_2_DEP,
  arrival: FLIGHT_POINT_2_2_ARR,
  airline: "Ryanair",
  airlineIATA: "FR",
}

export const FLIGHT_2_3: Flight = {
  number: "XC-7536",
  departure: FLIGHT_POINT_2_3_DEP,
  arrival: FLIGHT_POINT_2_3_ARR,
  airline: "Corendon Airlines",
  airlineIATA: "XC",
}