import './SearchPage.scss';
import React from "react";
import Navbar from "components/navbar/Navbar";
import SearchBar from "components/search-bar/SearchBar";
import {TicketSearchResults} from "components/ticket-search-results/TicketSearchResults";
import {TICKETS} from "mocks/search/ticketSearchResults";

const SearchPage = () => {
  return (
      <div>
        <Navbar/>
        <header className="page-title">
          <h1>Авиабилеты</h1>
        </header>
        <SearchBar/>
        <TicketSearchResults tickets={TICKETS}/>
      </div>
  );
}

export default SearchPage;