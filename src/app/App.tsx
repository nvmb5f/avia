import React from 'react';
import './App.scss'
import SearchPage from "./pages/search-page/SearchPage";

const App = () => <SearchPage/>

export default App;
