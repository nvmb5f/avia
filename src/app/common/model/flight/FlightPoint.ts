export interface FlightPoint {
  date: Date;
  country: string;
  city: string;
  airport: string;
  airportIATA: string;
}
