import {BaggagePolicy} from "models/baggage/BaggagePolicy";
import {Flight} from "models/flight/Flight";

export interface Ticket {
  price: number;
  flights: Flight[];
  baggagePolicies: BaggagePolicy[];
}
