import {FlightPoint} from "models/flight/FlightPoint";

// Airlines' logo: http://pics.avs.io/width/height/iata.png

export interface Flight {
  number: string;
  departure: FlightPoint;
  arrival: FlightPoint;
  airline: string;
  airlineIATA: string;
}