import {BaggageType} from "models/baggage/BaggageType";

export interface Baggage {
  type: BaggageType;
  price: number;
  places: number;
  maxWeight: number;
  maxLength: number;
  maxWidth: number;
  maxHeight: number;
}
