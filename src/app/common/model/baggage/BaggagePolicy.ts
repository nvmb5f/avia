import {Baggage} from "models/baggage/Baggage";

export type BaggagePolicy = Baggage[];
