import React from 'react';
import {DateInterval} from "models/types";

const Calendar = ({placeholder, from, to}: any & DateInterval) => {
  return (
      <div>
        <input type="date"
               placeholder={placeholder}
               min={from.toISOString().split("T")[0]}
               max={to.toISOString().split("T")[0]}/>
      </div>
  );
};

export default Calendar;
